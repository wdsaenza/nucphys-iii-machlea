import numpy as np
import matplotlib.pyplot as plt

printMap=False
printLine=True
time=0.105

def func(x):
    return np.sin(np.pi*x)

def print_u(u,j):
    for i in range(n+1):
        print(i*Deltax,j,u[i])
    print("")

n      = 10
Deltax = 1./n
alpha  = 0.01
Deltat = alpha*Deltax**2
u      = np.zeros(n+1)
unew   = np.zeros(n+1)

for i in range(1,len(u)-1):
    x = i*Deltax
    u[i] = func(x)

if(printMap):
    print_u(u,0)
    
for j in range(1,int(1./Deltat)):
    for i in range(1,len(u)-1):
        unew[i] = alpha*u[i-1] + (1.-2.*alpha)*u[i] + alpha*u[i+1]
    if( j*Deltat<=time and (j+1)*Deltat>time and printLine):
        print_u(u,j*Deltat)
    if(j%int(1./(100.*Deltat))==0 and printMap):
        print_u(u,j*Deltat)
    u=unew
    
if(printMap):   
    print_u(u,1)
