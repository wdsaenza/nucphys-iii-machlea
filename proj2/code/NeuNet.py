import numpy as np 
import tensorflow as tf
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error



np.random.seed(4155)
tf.set_random_seed(4155)

# Just to reset the graph such that it is possible to rerun this
# Jupyter cell without resetting the whole kernel.
tf.reset_default_graph()

Nx = 10
Nt = 200
x = np.linspace(0,1, Nx)
t = np.linspace(0,1, Nt)
xx, tt = np.meshgrid(x,t)
x = xx.ravel()
t = tt.ravel()

x_tf = tf.convert_to_tensor(x.reshape(-1,1),dtype=tf.float64)
t_tf = tf.convert_to_tensor(t.reshape(-1,1),dtype=tf.float64)

num_iter = int(1e4)

#fin cost for 10 = 0.239206
#fin cost for 20 = 0.00299336
Neurons = [20]

num_hidden_neurons = [Neurons[0]]
num_hidden_layers = len(num_hidden_neurons)

# Input layer
previous_layer = tf.concat([x_tf,t_tf],1)
    
# Hidden layers
for l in range(num_hidden_layers):
    current_layer = tf.layers.dense(previous_layer,
                                    num_hidden_neurons[l],
                                    activation=tf.nn.sigmoid)
    previous_layer = current_layer
    
# Output layer
dnn_output = tf.layers.dense(previous_layer, 1)

# functions involved in the differential equation
g_trial = tf.sin(np.pi*x_tf)*3**(dnn_output*t_tf)
dx_g_trial = tf.gradients(g_trial,x_tf)
d2x_g_trial = tf.gradients(dx_g_trial,x_tf)
dt_g_trial = tf.gradients(g_trial,t_tf)

# Cost function
err = tf.square( d2x_g_trial[0] - dt_g_trial[0])
cost = tf.reduce_sum(err)

learning_rate_4_Adam = 0.01
learning_rate_4_GDO  = 0.00001

# optimizing method
optimizer = tf.train.GradientDescentOptimizer(learning_rate_4_GDO)
#optimizer = tf.train.AdamOptimizer(learning_rate_4_Adam)
traning_op = optimizer.minimize(cost)

init = tf.global_variables_initializer()

with tf.Session() as sess:
    init.run()
    
    # Evaluate the initial cost:
    print('Initial cost: %g'%cost.eval())
    
    for i in range(num_iter):
        sess.run(traning_op)
        if(i%1000==0):
            print("iter = ",i," cost = ",cost.eval())
    
    print('Final cost: %g'%cost.eval())
    
    g_dnn_tf_descent = g_trial.eval()



def an_sol(x,t):
    return np.sin(np.pi*x)*np.exp(-np.pi*np.pi*t)


amax = np.argmax(np.abs(g_dnn_tf_descent))
norm = g_dnn_tf_descent.flat[amax]
                            
    
# Points for plot ----------------------------#
Min=0
Max=1
x_p = np.arange(0, 1, (Max-Min)/(Nx))
t_p = np.arange(0, 1, (Max-Min)/(Nt))

x_p, t_p = np.meshgrid(x_p,t_p)
u_p = an_sol(x_p,t_p)


R2  = r2_score(g_dnn_tf_descent.flatten(),u_p.flatten())
MSE = mean_squared_error(g_dnn_tf_descent.flatten(),u_p.flatten())



fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(x_p,
                       t_p,
                       u_p,
                       cmap=cm.coolwarm,linewidth=0, antialiased=False)
g_dnn_tf_descent=g_dnn_tf_descent/norm
ax.scatter(x,
           t,
           g_dnn_tf_descent.reshape(*xx.shape))
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()



