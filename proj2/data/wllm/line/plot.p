file1a='adam0_0t-200Dt.dat'
file2a='adam0_5t-200Dt.dat'
file3a='adam0_9t-200Dt.dat'
file1b='gdo0_0t-200Dt.dat'
file2b='gdo0_5t-200Dt.dat'
file3b='gdo0_9t-200Dt.dat'

set key box
set key width -1
set key opaque
set key spacing 0.75
#set terminal epslatex size 3.5,2.5 color colortext
#set output 'time-lines.tex'

set xl '$x$'
set yl '$u_{\text{neu}}(x,t)/u_{\text{ana}}(x,t)$'
set xr [0:1]
set yr [0.7:1.2]
set ytics 0.1

time1=0.0
time2=0.50251256
time3=0.90452261

f(x,t)=sin(pi*x)*exp(-t*pi*pi)
mlw = 15
mps = 2
plot file1a u 1:(($2)/(f(($1),time1))) every ::::8 with dots lw mlw lc 7 notitle,\
     file1a u 1:(($2)/(f(($1),time1))) every ::::8 with lines lw 3 lc 7 t '\tiny{$t=0$}',\
     file1b u 1:(($2)/(f(($1),time1))) every ::::8 with points pt 6 ps mps lc 7 notitle,\
     file1b u 1:(($2)/(f(($1),time1))) every ::::8 with lines lw 3 lc 7 notitle,\
     file2a u 1:(($2)/($3)) every ::::8 with dots lw mlw lc 3 notitle,\
     file2a u 1:(($2)/(f(($1),time2))) every ::::8 with lines lw 3 lc 3 t '\tiny{$=.5$}',\
     file2b u 1:(($2)/(f(($1),time2))) every ::::8 with points pt 6 ps mps lc 3 notitle,\
     file2b u 1:(($2)/(f(($1),time2))) every ::::8 with lines lw 3 lc 3 notitle,\
     file3a u 1:(($2)/(f(($1),time3))) every ::::8 with dots lw mlw lc 4 notitle,\
     file3a u 1:(($2)/(f(($1),time3))) every ::::8 with lines lw 3 lc 4 t '\tiny{$=.9$}',\
     file3b u 1:(($2)/(f(($1),time3))) every ::::8 with points pt 6 ps mps lc 4 notitle,\
     file3b u 1:(($2)/(f(($1),time3))) every ::::8 with lines lw 3 lc 4 notitle,\
     1 w l dashtype 3 lw 3 lc 8 notitle
# plot file1a ,\
#      file1b ,\
#      f(x,time1)
# plot file2a ,\
#      file2a u 1:3,\
#      file2b ,\
#      f(x,time2)
# plot file3a ps 5,\
#      file3a u 1:3,\
#      file3b ,\
#      file3b u 1:3,\
#      f(x,time3)
