from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from random import random, seed
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn import linear_model


# Definition of parameter for the calculus ---------------#
np.random.seed(4155)
# Make data.
Min=0          # Min range
Max=1          # Max range
Npoints = 40000 # Number of points
print("# Number of points: ",Npoints)
# --------------------------------------------------------#

# Definition of Franke function --------------#
def FrankeFunction(x,y):
    term1 = 0.75*np.exp(-(0.25*(9*x-2)**2) - 0.25*((9*y-2)**2))
    term2 = 0.75*np.exp(-((9*x+1)**2)/49.0 - 0.1*(9*y+1))
    term3 = 0.5*np.exp(-(9*x-7)**2/4.0 - 0.25*((9*y-3)**2))
    term4 = -0.2*np.exp(-(9*x-4)**2 - (9*y-7)**2)
    return term1 + term2 + term3 + term4
# --------------------------------------------#
#---------------------------------------------#

# Points of 'measurements' ------------------------#
Noise   = 0.1
print("# Noise: ",Noise)
x_data = np.random.rand(Npoints,1)
y_data = np.random.rand(Npoints,1)
z_data = FrankeFunction(x_data,y_data) + Noise*np.random.randn(Npoints,1)

x_data=x_data.T[0]
y_data=y_data.T[0]
z_data=z_data.T[0]

# Ridge Implementation -------------------------------------------------#
def RidgeWithCross(X_data,Y_data,Z_data,MaxOrder,Nsplits,lmb):
    print("#")
    print("# -- Ridge with cross validation --")
    print("# Nsplits = ",Nsplits," lambda = ",lmb)
    print("#")
    print("# Max order", "R2","MSE",sep="\t")
    kfold= KFold(n_splits=Nsplits)
    ind=np.empty(MaxOrder)
    R2 =np.empty(MaxOrder)
    MSE=np.empty(MaxOrder)
    for i in range(1,MaxOrder+1):
        Poly = PolynomialFeatures(degree=i)
        XY   = Poly.fit_transform(np.c_[X_data,Y_data])
        MeanR2  = np.mean(cross_val_score(Ridge(alpha=lmb),
                                          XY,
                                          Z_data,
                                          scoring="r2",
                                          cv=kfold))
        MeanMSE = np.mean(cross_val_score(Ridge(alpha=lmb),
                                          XY,
                                          Z_data,
                                          scoring="neg_mean_squared_error",
                                          cv=kfold))
        ind[i-1] = i
        R2[i-1]  = MeanR2
        MSE[i-1] = MeanMSE
        #return ind,R2,-MSE
        print(i,MeanR2,-MeanMSE,sep="\t")
    print("\n")

def RidgeSimple(X_data,Y_data,Z_data,MaxOrder,lmb):
    print("#")
    print("# -- Ridge with cross validation --")
    print("# lambda = ",lmb)
    print("#")
    print("# Max order", "R2","MSE",sep="\t")
    ind=np.empty(MaxOrder)
    R2 =np.empty(MaxOrder)
    MSE=np.empty(MaxOrder)
    for i in range(1,MaxOrder+1):
        Elements = int((i+1)*(i+2)/2)
        Poly = PolynomialFeatures(degree=i)
        XY   = Poly.fit_transform(np.c_[X_data,Y_data])
        ridge=linear_model.Ridge(alpha=lmb)
        ridge.fit(XY,z_data)
        beta_ridge = np.zeros(Elements)
        Iden = np.eye(Elements)
        beta_ridge = (np.linalg.inv( XY.T.dot(XY) + lmb*Iden).dot(XY.T).dot(z_data)).flatten()      
        Zpredict = XY.dot(beta_ridge)
        ind[i-1] = i
        R2[i-1]  = r2_score(Z_data,Zpredict)
        MSE[i-1] = mean_squared_error(Z_data,Zpredict)
        #return ind,R2,MSE
        print(i,R2[i-1],MSE[i-1],sep="\t")
    print("\n")
# ----------------------------------------------------------------------#
# Ridge calling --------------------------------------------------------#

M_Order=5
RidgeSimple(x_data,y_data,z_data,M_Order,0.1)
RidgeWithCross(x_data,y_data,z_data,M_Order,6,0.1)



