from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from random import random, seed
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.linear_model import RidgeCV, LassoCV
from sklearn import linear_model


# Definition of parameter for the calculus ---------------#
np.random.seed(4155)
# Make data.
Min=0          # Min range
Max=1          # Max range
Npoints = 50  #Number of points
print("# Number of points: ",Npoints)
# --------------------------------------------------------#

# Definition of Franke function --------------#
def FrankeFunction(x,y):
    term1 = 0.75*np.exp(-(0.25*(9*x-2)**2) - 0.25*((9*y-2)**2))
    term2 = 0.75*np.exp(-((9*x+1)**2)/49.0 - 0.1*(9*y+1))
    term3 = 0.5*np.exp(-(9*x-7)**2/4.0 - 0.25*((9*y-3)**2))
    term4 = -0.2*np.exp(-(9*x-4)**2 - (9*y-7)**2)
    return term1 + term2 + term3 + term4
# --------------------------------------------#
#---------------------------------------------#

# Points of 'measurements' ------------------------#
Noise   = 0.15
print("# Noise: ",Noise)
x_data = np.random.rand(Npoints,1)
y_data = np.random.rand(Npoints,1)
z_data = FrankeFunction(x_data,y_data) + Noise*np.random.randn(Npoints,1)

x_data=x_data.T[0]
y_data=y_data.T[0]
z_data=z_data.T[0]

# -------------------------------------------------#
def OLSWithCross(X_data,Y_data,Z_data,MaxOrder,Nsplits):
    kfold= KFold(n_splits=Nsplits)
    Poly = PolynomialFeatures(degree=MaxOrder)
    XY   = Poly.fit_transform(np.c_[X_data,Y_data])
    MeanMSE = np.mean(cross_val_score(LinearRegression(),
                                      XY,
                                      Z_data,
                                      scoring="neg_mean_squared_error",
                                      cv=kfold))
    return -MeanMSE


def OLSSimple(X_data,Y_data,Z_data,MaxOrder):
    Poly = PolynomialFeatures(degree=MaxOrder)
    XY   = Poly.fit_transform(np.c_[X_data,Y_data])
    X_ols = np.c_[ XY ]
    beta  = np.linalg.inv(X_ols.T.dot(X_ols)).dot(X_ols.T).dot(Z_data)    
    Zpredict = X_ols.dot(beta)
    MSE = mean_squared_error(Z_data,Zpredict)
    return MSE

        
# Ridge Implementation -------------------------------------------------#
def RidgeWithCross(X_data,Y_data,Z_data,MaxOrder,Nsplits,lmb):
    kfold= KFold(n_splits=Nsplits)
    Poly = PolynomialFeatures(degree=MaxOrder)
    XY   = Poly.fit_transform(np.c_[X_data,Y_data])
    MeanR2  = np.mean(cross_val_score(Ridge(alpha=lmb),
                                      XY,
                                      Z_data,
                                      scoring="r2",
                                      cv=kfold))
    MeanMSE = np.mean(cross_val_score(Ridge(alpha=lmb),
                                      XY,
                                      Z_data,
                                      scoring="neg_mean_squared_error",
                                      cv=kfold))
    return -MeanMSE

def RidgeSimple(X_data,Y_data,Z_data,MaxOrder,lmb):
    i=MaxOrder
    Elements = int((i+1)*(i+2)/2)
    Poly = PolynomialFeatures(degree=i)
    XY   = Poly.fit_transform(np.c_[X_data,Y_data])
    ridge=linear_model.Ridge(alpha=lmb)
    ridge.fit(XY,Z_data)
    beta_ridge = np.zeros(Elements)
    Iden = np.eye(Elements)
    beta_ridge = (np.linalg.inv( XY.T.dot(XY) + lmb*Iden).dot(XY.T).dot(Z_data)).flatten()      
    Zpredict = XY.dot(beta_ridge)
    MSE = mean_squared_error(Z_data,Zpredict)
    return MSE

# Lasso Implementation -------------------------------------------------#
def LassoWithCross(X_data,Y_data,Z_data,MaxOrder,Nsplits,lmb):
    kfold= KFold(n_splits=Nsplits)
    i = MaxOrder
    Poly = PolynomialFeatures(degree=i)
    XY   = Poly.fit_transform(np.c_[X_data,Y_data])
    MeanMSE = np.mean(cross_val_score(Lasso(alpha=lmb),
                                      XY,
                                      Z_data,
                                      scoring="neg_mean_squared_error",
                                      cv=kfold))
    return -MeanMSE


def LassoSimple(X_data,Y_data,Z_data,MaxOrder,lmb):
    i = MaxOrder
    Elements = int((i+1)*(i+2)/2)
    Poly = PolynomialFeatures(degree=i)
    XY   = Poly.fit_transform(np.c_[X_data,Y_data])
    lasso=linear_model.Lasso(alpha=lmb)
    lasso.fit(XY,Z_data)
    beta_lasso=lasso.coef_
    Zpredict = XY.dot(beta_lasso)
    MSE = mean_squared_error(Z_data,Zpredict)
    return MSE


M_Order=5
CV=5
print("# polynomial order = ",M_Order)
print("# k-fold = ",CV)
# ----------------------------------------------------------------------#
# OLS calling ----------------------------------------------------------#
mse_ols = OLSSimple(x_data,y_data,z_data,M_Order)
mse_ols_cv = OLSWithCross(x_data,y_data,z_data,M_Order,CV)
# ----------------------------------------------------------------------#
# Ridge calling --------------------------------------------------------#
'''
lambdas=np.logspace(-10,1,num=4e2)
for i in range(len(lambdas)):
    print(lambdas[i],
          RidgeWithCross(x_data,y_data,z_data,M_Order,CV,lambdas[i]),
          LassoWithCross(x_data,y_data,z_data,M_Order,CV,lambdas[i]),
          mse_ols_cv,
          sep='\t')
'''
#RidgeWithCross(x_data,y_data,z_data,M_Order,CrossVa,1e-1)

# ----------------------------------------------------------------------#
# Lasso calling --------------------------------------------------------#

#LassoSimple(x_data,y_data,z_data,M_Order,0.001)
#LassoWithCross(x_data,y_data,z_data,M_Order,CrossVa,1e-1)


Degree   = 5
Elements = int((Degree+1)*(Degree+2)/2)
Poly  = PolynomialFeatures(degree=Degree)
XY    = Poly.fit_transform(np.c_[x_data,y_data])
XY = np.c_[ XY ]


reg=LinearRegression()
reg.fit(XY, z_data)

Myl=np.logspace(-10,1,num=2e2)


for i in range(len(Myl)-1):
    ridge=linear_model.RidgeCV(alphas=(Myl[i],Myl[i+1]),
                               cv=5)
    ridge.fit(XY,z_data)
    print(ridge.alpha_,
          ridge.coef_[0],
          ridge.coef_[1],
          ridge.coef_[2],
          ridge.coef_[3],
          ridge.coef_[4],
          ridge.coef_[5],
          ridge.coef_[6],
          ridge.coef_[7],
          ridge.coef_[8],
          ridge.coef_[9],
          ridge.coef_[10],
          ridge.coef_[11],
          ridge.coef_[12],
          ridge.coef_[13],
          ridge.coef_[14],
          ridge.coef_[15],
          ridge.coef_[16],
          ridge.coef_[17],
          ridge.coef_[18],
          ridge.coef_[19],
          ridge.coef_[20],
          sep='\t')
'''
for i in range(len(Myl)-1):
    lasso=linear_model.LassoCV(alphas=(Myl[i],Myl[i+1]),
                               cv=5)
    lasso.fit(XY,z_data)
    print(lasso.alpha_,
          lasso.coef_[0],
          lasso.coef_[1],
          lasso.coef_[2],
          lasso.coef_[3],
          lasso.coef_[4],
          lasso.coef_[5],
          lasso.coef_[6],
          lasso.coef_[7],
          lasso.coef_[8],
          lasso.coef_[9],
          lasso.coef_[10],
          lasso.coef_[11],
          lasso.coef_[12],
          lasso.coef_[13],
          lasso.coef_[14],
          lasso.coef_[15],
          lasso.coef_[16],
          lasso.coef_[17],
          lasso.coef_[18],
          lasso.coef_[19],
          lasso.coef_[20],
          sep='\t')



'''
