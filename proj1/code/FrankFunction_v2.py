from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from random import random, seed
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression

# Definition of parameter for the calculus ---------------#
np.random.seed(4155)

fig = plt.figure()
# Make data.
Min=0          # Min range
Max=1          # Max range
Npoints = 400 # Number of points
print("# Number of points: ",Npoints)
# --------------------------------------------------------#

# Points for plot ----------------------------#
#x = np.arange(0, 1, (Max-Min)/(Npoints))
#y = np.arange(0, 1, (Max-Min)/(Npoints))
#x, y = np.meshgrid(x,y)
# --------------------------------------------#


# Definition of Franke function --------------#
def FrankeFunction(x,y):
    term1 = 0.75*np.exp(-(0.25*(9*x-2)**2) - 0.25*((9*y-2)**2))
    term2 = 0.75*np.exp(-((9*x+1)**2)/49.0 - 0.1*(9*y+1))
    term3 = 0.5*np.exp(-(9*x-7)**2/4.0 - 0.25*((9*y-3)**2))
    term4 = -0.2*np.exp(-(9*x-4)**2 - (9*y-7)**2)
    return term1 + term2 + term3 + term4

#z = FrankeFunction(x, y)
# --------------------------------------------#

# Plot of function with 'points for plot'-----#

#ax = fig.gca(projection='3d')
# Plot the surface.
#surf = ax.plot_surface(x, y, z, cmap=cm.coolwarm,linewidth=0, antialiased=False)
# Customize the z axis.
#ax.set_zlim(-0.10, 1.40)
#ax.zaxis.set_major_locator(LinearLocator(10))
#ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
# Add a color bar which maps values to colors.
#fig.colorbar(surf, shrink=0.5, aspect=5)
#plt.show()

#---------------------------------------------#


# Points of 'measurements' ------------------------#
Noise   = 0.1
print("# Noise: ",Noise)
x_data = np.random.rand(Npoints,1)
y_data = np.random.rand(Npoints,1)
z_data = FrankeFunction(x_data,y_data) + Noise*np.random.randn(Npoints,1)

x_data=x_data.T[0]
y_data=y_data.T[0]
z_data=z_data.T[0]

# -------------------------------------------------#
def OLSWithCross(X_data,Y_data,Z_data,MaxOrder,Nsplits):
    print("#")
    print("# -- OLS with cross validation --")
    print("# Nsplits = ",Nsplits)
    print("#")
    print("# Max order", "R2","MSE",sep="\t")
    kfold= KFold(n_splits=Nsplits)
    ind=np.empty(MaxOrder)
    R2 =np.empty(MaxOrder)
    MSE=np.empty(MaxOrder)
    for i in range(1,MaxOrder+1):
        Poly = PolynomialFeatures(degree=i)
        XY   = Poly.fit_transform(np.c_[X_data,Y_data])
        MeanR2  = np.mean(cross_val_score(LinearRegression(),
                                          XY,
                                          Z_data,
                                          scoring="r2",
                                          cv=kfold))
        MeanMSE = np.mean(cross_val_score(LinearRegression(),
                                          XY,
                                          Z_data,
                                          scoring="neg_mean_squared_error",
                                          cv=kfold))
        ind[i-1] = i
        R2[i-1]  = MeanR2
        MSE[i-1] = MeanMSE
        #return ind,R2,-MSE
        print(i,MeanR2,-MeanMSE,sep="\t")

def OLSSimple(X_data,Y_data,Z_data,MaxOrder):
    print("#")
    print("# -- OLS simple --")
    print("#")
    print("#")
    print("# Max order", "R2","MSE",sep="\t")
    ind=np.empty(MaxOrder)
    R2 =np.empty(MaxOrder)
    MSE=np.empty(MaxOrder)
    for i in range(1,MaxOrder+1):
        Poly = PolynomialFeatures(degree=i)
        XY   = Poly.fit_transform(np.c_[X_data,Y_data])
        X_ols = np.c_[ XY ]
        beta  = np.linalg.inv(X_ols.T.dot(X_ols)).dot(X_ols.T).dot(Z_data)    
        Zpredict = X_ols.dot(beta)
        ind[i-1] = i
        R2[i-1]  = r2_score(Z_data,Zpredict)
        MSE[i-1] = mean_squared_error(Z_data,Zpredict)
        #return ind,R2,MSE
        print(i,R2[i-1],MSE[i-1],sep="\t")

        
M_Order=7
OLSSimple(x_data,y_data,z_data,M_Order)
#OLSWithCross(x_data,y_data,z_data,M_Order,3)



# Chunck to extract Beta variances -----------------------------#
Degree= 5
Poly  = PolynomialFeatures(degree=Degree)
XY    = Poly.fit_transform(np.c_[x_data,y_data])
X_ols = np.c_[ XY ]
beta  = np.linalg.inv(X_ols.T.dot(X_ols)).dot(X_ols.T).dot(z_data)
Sigma = 1
Var_B = Sigma*np.linalg.inv(X_ols.T.dot(X_ols))
Names = Poly.get_feature_names()
'''
print('name', 'beta', 'Var_beta',sep="\t\t")
for i in range(len(Names)):
    print(Names[i], beta[i], np.sqrt(Var_B[i][i]),sep="\t\t")
'''
# ---------------------------------------------------------------#
'''
plt.plot(ind[0].T,R2[0].T,'.')
plt.show()
'''

# Plot of points 'Data'-------------------------------------#
'''
ax = plt.axes(projection='3d')
ax.scatter(x_data, y_data, z_data)
plt.show()
'''
# ----------------------------------------------------------#

