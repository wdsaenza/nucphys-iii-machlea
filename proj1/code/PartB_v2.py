from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from random import random, seed
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn import linear_model


# Definition of parameter for the calculus ---------------#
np.random.seed(4155)
# Make data.
Min=0          # Min range
Max=1          # Max range
Npoints = 20  # Number of points
print("# Number of points: ",Npoints)
# --------------------------------------------------------#

# Definition of Franke function --------------#
def FrankeFunction(x,y):
    term1 = 0.75*np.exp(-(0.25*(9*x-2)**2) - 0.25*((9*y-2)**2))
    term2 = 0.75*np.exp(-((9*x+1)**2)/49.0 - 0.1*(9*y+1))
    term3 = 0.5*np.exp(-(9*x-7)**2/4.0 - 0.25*((9*y-3)**2))
    term4 = -0.2*np.exp(-(9*x-4)**2 - (9*y-7)**2)
    return term1 + term2 + term3 + term4
# --------------------------------------------#
#---------------------------------------------#

# Points of 'measurements' ------------------------#
Noise   = 0.1
print("# Noise: ",Noise)
x_data = np.random.rand(Npoints,1)
y_data = np.random.rand(Npoints,1)
z_data = FrankeFunction(x_data,y_data) + Noise*np.random.randn(Npoints,1)

x_data=x_data.T[0]
y_data=y_data.T[0]
z_data=z_data.T[0]

# -------------------------------------------------#
def OLSWithCross(X_data,Y_data,Z_data,MaxOrder,Nsplits):
    print("#")
    print("# -- OLS with cross validation --")
    print("# Nsplits = ",Nsplits)
    print("#")
    print("# Max order", "R2","MSE",sep="\t")
    kfold= KFold(n_splits=Nsplits)
    ind=np.empty(MaxOrder)
    R2 =np.empty(MaxOrder)
    MSE=np.empty(MaxOrder)
    for i in range(1,MaxOrder+1):
        Poly = PolynomialFeatures(degree=i)
        XY   = Poly.fit_transform(np.c_[X_data,Y_data])
        MeanR2  = np.mean(cross_val_score(LinearRegression(),
                                          XY,
                                          Z_data,
                                          scoring="r2",
                                          cv=kfold))
        MeanMSE = np.mean(cross_val_score(LinearRegression(),
                                          XY,
                                          Z_data,
                                          scoring="neg_mean_squared_error",
                                          cv=kfold))
        ind[i-1] = i
        R2[i-1]  = MeanR2
        MSE[i-1] = MeanMSE
        #return ind,R2,-MSE
        print(i,MeanR2,-MeanMSE,sep="\t")

def OLSSimple(X_data,Y_data,Z_data,MaxOrder):
    print("#")
    print("# -- OLS simple --")
    print("#")
    print("#")
    print("# Max order", "R2","MSE",sep="\t")
    ind=np.empty(MaxOrder)
    R2 =np.empty(MaxOrder)
    MSE=np.empty(MaxOrder)
    for i in range(1,MaxOrder+1):
        Poly = PolynomialFeatures(degree=i)
        XY   = Poly.fit_transform(np.c_[X_data,Y_data])
        X_ols = np.c_[ XY ]
        beta  = np.linalg.inv(X_ols.T.dot(X_ols)).dot(X_ols.T).dot(Z_data)    
        Zpredict = X_ols.dot(beta)
        ind[i-1] = i
        R2[i-1]  = r2_score(Z_data,Zpredict)
        MSE[i-1] = mean_squared_error(Z_data,Zpredict)
        #return ind,R2,MSE
        print(i,R2[i-1],MSE[i-1],sep="\t")

        
# Ridge Implementation -------------------------------------------------#
def RidgeWithCross(X_data,Y_data,Z_data,MaxOrder,Nsplits,lmb):
    # print("#")
    # print("# -- Ridge with cross validation --")
    # print("# Nsplits = ",Nsplits," lambda = ",lmb)
    # print("#")
    # print("# Max order", "R2","MSE",sep="\t")
    kfold= KFold(n_splits=Nsplits)
    ind=np.empty(MaxOrder)
    R2 =np.empty(MaxOrder)
    MSE=np.empty(MaxOrder)
    for i in range(1,MaxOrder+1):
        Poly = PolynomialFeatures(degree=i)
        XY   = Poly.fit_transform(np.c_[X_data,Y_data])
        MeanR2  = np.mean(cross_val_score(Ridge(alpha=lmb),
                                          XY,
                                          Z_data,
                                          scoring="r2",
                                          cv=kfold))
        MeanMSE = np.mean(cross_val_score(Ridge(alpha=lmb),
                                          XY,
                                          Z_data,
                                          scoring="neg_mean_squared_error",
                                          cv=kfold))
        ind[i-1] = i
        R2[i-1]  = MeanR2
        MSE[i-1] = MeanMSE
    #     print(i,MeanR2,-MeanMSE,sep="\t")
    # print("\n")
    return -MSE

def RidgeSimple(X_data,Y_data,Z_data,MaxOrder,lmb):
    # print("#")
    # print("# -- Ridge simple --")
    # print("# lambda = ",lmb)
    # print("#")
    # print("# Max order", "R2","MSE",sep="\t")
    ind=np.empty(MaxOrder)
    R2 =np.empty(MaxOrder)
    MSE=np.empty(MaxOrder)
    for i in range(1,MaxOrder+1):
        Elements = int((i+1)*(i+2)/2)
        Poly = PolynomialFeatures(degree=i)
        XY   = Poly.fit_transform(np.c_[X_data,Y_data])
        ridge=linear_model.Ridge(alpha=lmb)
        ridge.fit(XY,Z_data)
        beta_ridge = np.zeros(Elements)
        Iden = np.eye(Elements)
        beta_ridge = (np.linalg.inv( XY.T.dot(XY) + lmb*Iden).dot(XY.T).dot(Z_data)).flatten()      
        Zpredict = XY.dot(beta_ridge)
        ind[i-1] = i
        R2[i-1]  = r2_score(Z_data,Zpredict)
        MSE[i-1] = mean_squared_error(Z_data,Zpredict)
    #     print(i,R2[i-1],MSE[i-1],sep="\t")
    # print("\n")
    return MSE

# Lasso Implementation -------------------------------------------------#
def LassoWithCross(X_data,Y_data,Z_data,MaxOrder,Nsplits,lmb):
    print("#")
    print("# -- Lasso with cross validation --")
    print("# Nsplits = ",Nsplits," lambda = ",lmb)
    print("#")
    print("# Max order", "R2","MSE",sep="\t")
    kfold= KFold(n_splits=Nsplits)
    ind=np.empty(MaxOrder)
    R2 =np.empty(MaxOrder)
    MSE=np.empty(MaxOrder)
    for i in range(1,MaxOrder+1):
        Poly = PolynomialFeatures(degree=i)
        XY   = Poly.fit_transform(np.c_[X_data,Y_data])
        MeanR2  = np.mean(cross_val_score(Ridge(alpha=lmb),
                                          XY,
                                          Z_data,
                                          scoring="r2",
                                          cv=kfold))
        MeanMSE = np.mean(cross_val_score(Lasso(alpha=lmb),
                                          XY,
                                          Z_data,
                                          scoring="neg_mean_squared_error",
                                          cv=kfold))
        ind[i-1] = i
        R2[i-1]  = MeanR2
        MSE[i-1] = MeanMSE
        #return ind,R2,-MSE
        print(i,MeanR2,-MeanMSE,sep="\t")
    print("\n")

def LassoSimple(X_data,Y_data,Z_data,MaxOrder,lmb):
    print("#")
    print("# -- Lasso simple --")
    print("# lambda = ",lmb)
    print("#")
    print("# Max order", "R2","MSE",sep="\t")
    ind=np.empty(MaxOrder)
    R2 =np.empty(MaxOrder)
    MSE=np.empty(MaxOrder)
    for i in range(1,MaxOrder+1):
        Elements = int((i+1)*(i+2)/2)
        Poly = PolynomialFeatures(degree=i)
        XY   = Poly.fit_transform(np.c_[X_data,Y_data])
        lasso=linear_model.Lasso(alpha=lmb)
        lasso.fit(XY,Z_data)
        beta_lasso=lasso.coef_
        Zpredict = XY.dot(beta_lasso)
        ind[i-1] = i
        R2[i-1]  = r2_score(Z_data,Zpredict)
        MSE[i-1] = mean_squared_error(Z_data,Zpredict)
        #return ind,R2,MSE
        print(i,R2[i-1],MSE[i-1],sep="\t")
    print("\n")
    M_Order=7

# ----------------------------------------------------------------------#
# Ridge calling --------------------------------------------------------#

#OLSSimple(x_data,y_data,z_data,M_Order)
#OLSWithCross(x_data,y_data,z_data,M_Order,3)

# ----------------------------------------------------------------------#
# Ridge calling --------------------------------------------------------#

M_Order=5
CrossVa=3

lambdas=np.logspace(-8,-2,num=2e2)
for i in range(len(lambdas)):
    print(lambdas[i],RidgeSimple(x_data,y_data,z_data,M_Order,lambdas[i])[4],sep='\t')
#RidgeWithCross(x_data,y_data,z_data,M_Order,CrossVa,1e-1)

# ----------------------------------------------------------------------#
# Lasso calling --------------------------------------------------------#

#LassoSimple(x_data,y_data,z_data,M_Order,0.001)
#LassoWithCross(x_data,y_data,z_data,M_Order,CrossVa,1e-1)


Degree   = 5
Elements = int((Degree+1)*(Degree+2)/2)
Poly  = PolynomialFeatures(degree=Degree)
XY    = Poly.fit_transform(np.c_[x_data,y_data])
XY = np.c_[ XY ]


reg=LinearRegression()
reg.fit(XY, z_data)

ridge=linear_model.Ridge(alpha=0.001)
ridge.fit(XY,z_data)

lasso=linear_model.Lasso(alpha=0.001)
lasso.fit(XY,z_data)



