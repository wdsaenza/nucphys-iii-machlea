from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from random import random, seed
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression

# Definition of parameter for the calculus ---------------#
np.random.seed(4155)

fig = plt.figure()
# Make data.
Min=0          # Min range
Max=1          # Max range
Npoints = 200  # Number of points

# --------------------------------------------------------#

# Points for plot ----------------------------#
x = np.arange(0, 1, (Max-Min)/(Npoints))
y = np.arange(0, 1, (Max-Min)/(Npoints))
x, y = np.meshgrid(x,y)
# --------------------------------------------#


# Definition of Franke function --------------#
def FrankeFunction(x,y):
    term1 = 0.75*np.exp(-(0.25*(9*x-2)**2) - 0.25*((9*y-2)**2))
    term2 = 0.75*np.exp(-((9*x+1)**2)/49.0 - 0.1*(9*y+1))
    term3 = 0.5*np.exp(-(9*x-7)**2/4.0 - 0.25*((9*y-3)**2))
    term4 = -0.2*np.exp(-(9*x-4)**2 - (9*y-7)**2)
    return term1 + term2 + term3 + term4

z = FrankeFunction(x, y)
# --------------------------------------------#

# Plot of function with 'points for plot'-----#
'''
ax = fig.gca(projection='3d')
# Plot the surface.
surf = ax.plot_surface(x, y, z, cmap=cm.coolwarm,linewidth=0, antialiased=False)
# Customize the z axis.
ax.set_zlim(-0.10, 1.40)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
# Add a color bar which maps values to colors.
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()
'''
#---------------------------------------------#


# Points of 'measurements' ------------------------#
Noise   = 0.1

x_data = np.random.rand(Npoints,1)
y_data = np.random.rand(Npoints,1)
z_data = FrankeFunction(x_data,y_data) + Noise*np.random.randn(Npoints,1)

x_data=x_data.T[0]
y_data=y_data.T[0]
z_data=z_data.T[0]

#x_train, x_test, y_train, y_test, z_train, z_test = train_test_split(x_data,
#                                                                     y_data,
#                                                                     z_data,
#                                                                     test_size=0.4,
#                                                                     random_state=0)

# -------------------------------------------------#

# Matrices with powers of x_data and y_data -------#
deg=5
polyX = PolynomialFeatures(degree=deg)
polyY = PolynomialFeatures(degree=deg)
X = polyX.fit_transform(x_data[:,np.newaxis])
Y = polyY.fit_transform(y_data[:,np.newaxis])
# -------------------------------------------------
# Matrix with power combination of x_data and y_data up to deg ------------#
XY_upTo = np.ones((len(x_data),int((deg+1)*(deg+2)/2)))
Pos_in_XY = 0
for i in range(deg+1):
    for j in range(deg+1-i):
        XY_upTo[:,Pos_in_XY] = X[:,j]*Y[:,i]
        Pos_in_XY+=1
#--------------------------------------------------------------------------------#
# Beta matrix and its variance ---------------------------#
X_ols = np.c_[ XY_upTo ]
beta  = np.linalg.inv(X_ols.T.dot(X_ols)).dot(X_ols.T).dot(z_data)    
Sigma = 1
Var_B = Sigma*np.linalg.inv(X_ols.T.dot(X_ols))
Zpredict = X_ols.dot(beta)


'''
for i in range(1,7):
    print(OLSUpTo(x_data,y_data,z_data,i))
'''
# Plot of points 'Data'-------------------------------------#
'''
ax = plt.axes(projection='3d')
ax.scatter(x_data, y_data, z_data)
plt.show()
'''
# ----------------------------------------------------------#

