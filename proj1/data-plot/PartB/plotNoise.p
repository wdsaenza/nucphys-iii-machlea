set logscale x
set xr [1e-9:1e1]
set x2r [1e-9:1e1]
set format x2 ''
set x2tics mirror
set key box
set key opaque
set key width -1.5
set key spacing 0.75
set key at graph 0.35,0.75

set terminal epslatex size 3.5,2.7 color colortext
set output 'ridge-lambda-noise.tex'

mytable5mse0_1noise='ridgeSimple-MSEvslambda-noise0_1-order5.txt'
set table mytable5mse0_1noise
plot for [i=0:8] 'ridgeSimple-noise0_1-lambda1e-'.i.'.dat' u (10**(-i)):3 every ::4::4
unset table

mytable5mse0_2noise='ridgeSimple-MSEvslambda-noise0_2-order5.txt'
set table mytable5mse0_2noise
plot for [i=0:8] 'ridgeSimple-noise0_2-lambda1e-'.i.'.dat' u (10**(-i)):3 every ::4::4
unset table

mytable5r20_1noise='ridgeSimple-R2vslambda-noise0_1-order5.txt'
set table mytable5r20_1noise
plot for [i=0:8] 'ridgeSimple-noise0_1-lambda1e-'.i.'.dat' u (10**(-i)):2 every ::4::4
unset table

mytable5r20_2noise='ridgeSimple-R2vslambda-noise0_2-order5.txt'
set table mytable5r20_2noise
plot for [i=0:8] 'ridgeSimple-noise0_2-lambda1e-'.i.'.dat' u (10**(-i)):2 every ::4::4
unset table

set macros
TMARGIN = "set tmargin at screen 0.96; set bmargin at screen 0.53"
MMARGIN = "set lmargin at screen 0.15; set rmargin at screen 0.95"
BMARGIN = "set tmargin at screen 0.53; set bmargin at screen 0.10"

set multiplot layout 2,1 rowsfirst

@TMARGIN; @MMARGIN
set yl 'MSE'
set format x ''
unset xl
set yr [0.005:0.05]
set ytics (0.01,0.02,0.03,0.04,0.05)
plot "< awk 'NF!=0 {print $0}' ".mytable5mse0_1noise."" u 1:2 with dots lw 15 lc 4 t '\tiny{$10\%$}',\
     "< awk 'NF!=0 {print $0}' ".mytable5mse0_1noise."" u 1:2 with lines lw 5 lc 4 notitle,\
     "< awk 'NF!=0 {print $0}' ".mytable5mse0_2noise."" u 1:2 with dots lw 15 lc 3 t '\tiny{$20\%$}',\
     "< awk 'NF!=0 {print $0}' ".mytable5mse0_2noise."" u 1:2 with lines lw 5 lc 3 notitle

@MMARGIN; @BMARGIN
set key at graph 0.35,0.55
set xl '$\lambda$'
set yl '$R^2$'
set format x "10$^{%L}$"
set yr [0.6:0.95]
set ytics (0.6,0.7,0.8,0.9)
set xtics (1e-9,1e-7,1e-5,1e-3,1e-1,1e1)
set x2r [1e-9:1e1]
set x2tics mirror
plot "< awk 'NF!=0 {print $0}' ".mytable5r20_1noise."" u 1:2 with dots lw 15 lc 4 t '\tiny{$10\%$}',\
     "< awk 'NF!=0 {print $0}' ".mytable5r20_1noise."" u 1:2 with lines lw 5 lc 4 notitle,\
     "< awk 'NF!=0 {print $0}' ".mytable5r20_2noise."" u 1:2 with dots lw 15 lc 3 t '\tiny{$20\%$}',\
     "< awk 'NF!=0 {print $0}' ".mytable5r20_2noise."" u 1:2 with lines lw 5 lc 3 notitle
