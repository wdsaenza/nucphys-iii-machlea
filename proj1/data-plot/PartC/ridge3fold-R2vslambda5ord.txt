
# Curve 0 of 5, 1 points
# Curve title: "'ridge3Fold-lambda1e'.i.'.dat' u (10**(i)):2 every ::4::4"
# x y type
 0.001  0.86994  i


# Curve 1 of 5, 1 points
# Curve title: "'ridge3Fold-lambda1e'.i.'.dat' u (10**(i)):2 every ::4::4"
# x y type
 0.01  0.861652  i


# Curve 2 of 5, 1 points
# Curve title: "'ridge3Fold-lambda1e'.i.'.dat' u (10**(i)):2 every ::4::4"
# x y type
 0.1  0.847119  i


# Curve 3 of 5, 1 points
# Curve title: "'ridge3Fold-lambda1e'.i.'.dat' u (10**(i)):2 every ::4::4"
# x y type
 1  0.816289  i


# Curve 4 of 5, 1 points
# Curve title: "'ridge3Fold-lambda1e'.i.'.dat' u (10**(i)):2 every ::4::4"
# x y type
 10  0.787592  i

