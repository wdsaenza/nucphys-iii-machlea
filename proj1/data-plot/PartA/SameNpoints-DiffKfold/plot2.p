file2='<paste R2-data_testOLS-2nsplits.txt R2-data_simpleOLS.txt'
file3='<paste R2-data_testOLS-3nsplits.txt R2-data_simpleOLS.txt'
file4='<paste R2-data_testOLS-4nsplits.txt R2-data_simpleOLS.txt'
file5='<paste R2-data_testOLS-5nsplits.txt R2-data_simpleOLS.txt'
file6='<paste R2-data_testOLS-6nsplits.txt R2-data_simpleOLS.txt'
file7='<paste R2-data_testOLS-7nsplits.txt R2-data_simpleOLS.txt'

set ytics 0.04

set yr [0.98:1.15]
set xr [0.5:7.6]
set x2r [0.5:7.6]

set key top left
set key box
set key width -1

unset xl
unset xtics
set x2tics format ''
set x2tics mirror
set yl '$MSE_{\text{k-Fold}}/MSE_{\text{OLS}}$'

set key opaque
set terminal epslatex size 4,4 color colortext
set output 'splits.tex'

set multiplot layout 2,1 rowsfirst
plot file2 u 1:(($3)/($6)) w dots lw 9 lc 2 notitle,\
     file2 u 1:(($3)/($6)) w l lw 3 lc 2 t '\tiny{2}',\
     file3 u 1:(($3)/($6)) w dots lw 9 lc 3 notitle,\
     file3 u 1:(($3)/($6)) w l lw 3 lc 3 t '\tiny{3}',\
     file4 u 1:(($3)/($6)) w dots lw 9 lc 4 notitle,\
     file4 u 1:(($3)/($6)) w l lw 3 lc 4 t '\tiny{4}',\
     file5 u 1:(($3)/($6)) w dots lw 9 lc 5 notitle,\
     file5 u 1:(($3)/($6)) w l lw 3 lc 5 t '\tiny{5}',\
     file6 u 1:(($3)/($6)) w dots lw 9 lc 6 notitle,\
     file6 u 1:(($3)/($6)) w l lw 3 lc 6 t '\tiny{6}',\
     1 w l dashtype 3 lw 6 lc 8 notitle

set format y '%.2f'
set ytics 0.01
unset x2tics
set xtics
set key bottom left
set yr [0.97:1.002]
set xl 'Maximum order of polynomial'
set yl '$R^2_{\text{k-Fold}}/R^2_{\text{OLS}}$'
plot file2 u 1:(($2)/($5)) w dots lw 9 lc 2 notitle,\
     file2 u 1:(($2)/($5)) w l lw 3 lc 2 t '\tiny{2}',\
     file3 u 1:(($2)/($5)) w dots lw 9 lc 3 notitle,\
     file3 u 1:(($2)/($5)) w l lw 3 lc 3 t '\tiny{3}',\
     file4 u 1:(($2)/($5)) w dots lw 9 lc 4 notitle,\
     file4 u 1:(($2)/($5)) w l lw 3 lc 4 t '\tiny{4}',\
     file5 u 1:(($2)/($5)) w dots lw 9 lc 5 notitle,\
     file5 u 1:(($2)/($5)) w l lw 3 lc 5 t '\tiny{5}',\
     file6 u 1:(($2)/($5)) w dots lw 9 lc 6 notitle,\
     file6 u 1:(($2)/($5)) w l lw 3 lc 6 t '\tiny{6}',\
     1 w l dashtype 3 lw 6 lc 8 notitle
