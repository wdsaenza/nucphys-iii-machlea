unset key

simpleMSEord5 = 0.011942263393413734
simpleR2ord5  = 0.8702992244751994

mytable5a='MSEolsCross-noise0_1-order5.txt'
set table mytable5a
plot for [i=2:13] 'olsCross'.i.'fold-1e5points-0_1noise.txt' u (i):3 every ::4::4
unset table

mytable5b='R2olsCross-noise0_1-order5.txt'
set table mytable5b
plot for [i=2:13] 'olsCross'.i.'fold-1e5points-0_1noise.txt' u (i):2 every ::4::4
unset table


set terminal epslatex size 3.5,2.7 color colortext
set output 'splits.tex'

set macros
TMARGIN = "set tmargin at screen 0.96; set bmargin at screen 0.53"
MMARGIN = "set lmargin at screen 0.15; set rmargin at screen 0.95"
BMARGIN = "set tmargin at screen 0.53; set bmargin at screen 0.10"

set multiplot layout 2,1 rowsfirst

@TMARGIN; @MMARGIN
set yl '$\text{MSE}_{\text{k-Fold}}$'
set xr [1.5:13.5]
set format x ''
unset xl
set yr [0.011946:0.011951]
set ytics (0.011947,0.011948,0.011949,0.011950)
plot "< awk 'NF!=0 {print $0}' ".mytable5a."" u 1:2 with dots lw 15 lc 7,\
     "< awk 'NF!=0 {print $0}' ".mytable5a."" u 1:2 with lines lw 5 lc 3

@MMARGIN; @BMARGIN
set xl '$k$'
set yl '$R^2_{\text{k-Fold}}$'
set format x '%1.f'
set yr [0.87020:0.87024]
set ytics (0.87020,0.87021,0.87022,0.87023)
plot "< awk 'NF!=0 {print $0}' ".mytable5b."" u 1:2 with dots lw 15 lc 7,\
     "< awk 'NF!=0 {print $0}' ".mytable5b."" u 1:2 with lines lw 5 lc 3
